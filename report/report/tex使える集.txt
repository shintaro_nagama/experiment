﻿TeX　使えるテンプレ

//セクション
\section*{課題}
\subsection*{}
\subsection*{}
\subsection*{}


//行間を縮め、文字をそのまま表示（プログラムコードなどに）
\begin{spacing}{0.7}
\begin{verbatim}
\end{verbatim}
\end{spacing}


//行間を縮め、2段組で文字をそのまま表示
\begin{spacing}{0.6}
\begin{multicols}{2}
\begin{verbatim}
\end{verbatim}
\end{multicols}
\end{spacing}

//箇条書き
\begin{itemize}
	\item 
	\item 
\end{itemize}


//図を表示する
図\ref{fig:}に示す．

\begin{figure}[H]
  \begin{center}
    \includegraphics[clip,width=cm]{}
    \caption{}
    \label{fig:}
  \end{center}
\end{figure}

//表を表示
\begin{table}[H]
  \begin{center}
    \caption{}
    \begin{tabular}{|c|c|c|c|} \hline
       &  &  &  \\ \hline \hline
       &  &  &  \\
       &  &  &  \\
       &  &  &  \\ \hline
    \end{tabular}
    \label{tab:}
  \end{center}
\end{table}