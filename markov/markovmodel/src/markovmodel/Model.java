package markovmodel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Model {
	protected List<String> sigma;
	protected int stnum;
	protected ArrayList<Double>[] states;
	protected double[][] deltas;
	
	protected List<Integer> statusAns;
	protected List<String> sigmaAns;
	
	public Model(){
		sigma = new ArrayList<String>();
		statusAns = new ArrayList<Integer>();
		sigmaAns = new ArrayList<String>();
	}
	
	public void modelCheck(){
		double x = 0.0;
		for(int i=0; i<stnum; i++){
			x=0.0;
			for(int j=0; j<states[i].size(); j++){
				x += (double)states[i].get(j);
			}
			if( x+1 == 2.0 || x+1 == 1.0 ){
				//問題なし
			}else if( x < 1.0){
				//問題あり
				System.out.println( states[i] + "の出現確率分布が合計で1.0になっていません");
				System.exit(0);  /* プログラムを終了する */
			}
		}
	}
	
	public void initializeDelta(){
		for(int j=0; j<stnum; j++){
			for(int k=0; k<stnum; k++){
				deltas[j][k] = 0.0;
			}
		}
	}
	
	public void initializeState(){
		for(int i=0; i<stnum; i++){
			states[i] = new ArrayList<Double>();
		}
	}
	
	public void initializeState2(){
		for(int i=0; i<stnum; i++){
			if(states[i].size() == 0){
				for(int j=0; j<sigma.size(); j++){
					states[i].add(0.0);
				}
			}	
		}
	}
	
	public void update(){
		
	}
	
	public void resetModel(){
		sigmaAns.removeAll(sigmaAns);
		statusAns.removeAll(statusAns);
	}
	
}
