package markovmodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class DoModel {
	private Model markovModel;
	protected Inference inf;
	protected int[] counter;
	protected Sfmt rnd;
	
	public DoModel(Model markovModel, Inference i){
		this.markovModel = markovModel;
		inf = i;
		counter = new int[markovModel.sigma.size()];
		
		for(int j=0; j<markovModel.sigma.size(); j++){
			counter[j] = 0;
		}
		int[] init_key = {(int) System.currentTimeMillis(), (int) Runtime.getRuntime().freeMemory()};
	    rnd = new Sfmt(init_key);
	}
	
	public void doModel(int num){
		int nowStatus = 0; //今の状態
		double ran, ranDice; //乱数による値
		double tmp = 0.0; //確率の状態遷移の確率を足していく
		double tmp2 = 0.0;
		int cnt = 0;
		markovModel.statusAns.add(0); //初期状態からの遷移
		
		
		while(cnt < num){
			//Math.random()は０以上１未満の乱数を返す
			//ran = Math.random();
			ran = rnd.NextUnif();
			tmp = 0.0;
			for(int j=0; j<markovModel.stnum; j++){
				tmp += markovModel.deltas[nowStatus][j];
				if( ran < tmp){
					//遷移する
					nowStatus = j;
					markovModel.statusAns.add(nowStatus);
					
					//今の状態が終端かどうかを確認するためのtmp
					tmp=0;
					for(int k=0; k<markovModel.stnum; k++){
						tmp += markovModel.deltas[nowStatus][k];
					}
					
					tmp2 = 0.0;
					for(int k=0; k<markovModel.states[nowStatus].size(); k++){
						tmp2 += markovModel.states[nowStatus].get(k);
					}
					
					if(tmp == 0){
						//終了状態へ遷移
						//System.exit(0);  /* プログラムを終了する */
					}else if(tmp2+1 == 2.0){
						//何らかのアクション
						//ran = Math.random();
						ran = rnd.NextUnif(); // 0以上1未満の実数
						tmp = 0.0;
						for(int k=0; k<markovModel.states[nowStatus].size(); k++){
							tmp += markovModel.states[nowStatus].get(k);
							if(ran < tmp){
								//System.out.println(markovModel.sigma.get(k));
								inf.w.add(markovModel.sigma.get(k));
								markovModel.sigmaAns.add(markovModel.sigma.get(k));
								counter[markovModel.sigma.indexOf(markovModel.sigma.get(k))] += 1;
								break;
							}
						}
						
					}
					break;
				}else{
					//遷移しない
				}
			}
			cnt++;
		}
		//System.out.println(num + "回終了しました");	
	}
	
	
	public void putC(){
		for(int i=0; i<markovModel.sigma.size(); i++){
			System.out.println(i + ": " + counter[i]);
		}
	}
	
	
	
}
