package markovmodel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Inference {
	protected Model markovModel;
	protected Study st;
	protected List<String> w; //出力された記号列
	protected List<Integer> ans;
	protected List<Integer> oldans;
	protected double[][] v; //確率
	protected int[][] vp; //番号保存
	protected Sfmt rnd;
	protected List<List<Double>> states;
	protected List<String> sigma;
	
	public Inference(Model m){
		markovModel = m;
		w = new ArrayList<String>();
		ans = new ArrayList<Integer>();
		oldans = new ArrayList<Integer>();
		states = new ArrayList<List<Double>>();
		sigma = new ArrayList<String>();
		
		int[] init_key = {(int) System.currentTimeMillis(), (int) Runtime.getRuntime().freeMemory()};
	    rnd = new Sfmt(init_key);
				
	}
	
	public void setSt(Study st){
		this.st = st;
	}
	
	public void initialize(){
		v = new double[markovModel.stnum][w.size()+2];
		vp = new int[markovModel.stnum][w.size()+2];
		v[0][0] = 1;
		for(int i=1; i<markovModel.stnum-1; i++){
			v[i][0] = 0;
		}
		for(int i=1; i<w.size()+2; i++){
			v[0][i] = 0;
		}
		
		for(int i=0; i<markovModel.stnum-1; i++){
			for(int j=0; j<w.size()+1; j++){
				vp[i][j] = 0;
			}
		}
	}
	
	public void inference(){
		initialize();
		for(int i=1; i<=w.size(); i++){
			for(int k=0; k<markovModel.stnum-1; k++){
				v[k][i] = checkValue(Math.log(markovModel.states[k].get(markovModel.sigma.indexOf(w.get(i-1))))) + checkValue(selectMax(k,i));
			}
		}
		getAns();
	}
	
	
	public void setRandomStates(){
		double sum=0.0;
		for(int i=0; i<markovModel.stnum; i++){
			List<Double> tmp = new ArrayList<Double>();
			for(int j=0; j<markovModel.sigma.size(); j++){
				tmp.add(0.0);
			}
			states.add(tmp);
		}
		
		for(int i=1; i<markovModel.stnum-1; i++){
			for(int j=0; j<markovModel.sigma.size(); j++){
				states.get(i).set(j, rnd.NextUnif());
				//states.get(i).set(j, (rnd.NextInt((int)(1.0-sum)*10000))/10000.0);
			}
		}
		
		for(int i=0; i<markovModel.stnum; i++){
			for(int j=0; j<markovModel.stnum; j++){
				st.deltas[i][j] = rnd.NextUnif();
			}
		}
		
		//合計で1になるように調整
		for(int i=0; i<markovModel.stnum; i++){
			sum = 0;
			for(int j=0; j<markovModel.stnum; j++){
				sum += st.deltas[i][j];
			}
			for(int j=0; j<markovModel.stnum; j++){
				st.deltas[i][j] = st.deltas[i][j]/sum;
			}
		}
		
		for(int i=0; i<markovModel.stnum; i++){
			sum = 0;
			for(int j=0; j<markovModel.sigma.size(); j++){
				sum += states.get(i).get(j);
			}
			for(int j=0; j<markovModel.sigma.size(); j++){
				if(sum!=0.0){
					states.get(i).set(j,states.get(i).get(j)/sum);
				}
				
			}
		}
		
		
	}
	
	public void update(){
		states.removeAll(states);
		for(int i=0; i<st.states.size(); i++){
			states.add(st.states.get(i));
		}
		
		oldans.removeAll(oldans);
		for(int i=0; i<ans.size(); i++){
			oldans.add(i, ans.get(i));
		}
		
	}
	
	
	
	public void inference2(){
		initialize();
		for(int i=1; i<=w.size(); i++){
			for(int k=0; k<markovModel.stnum-1; k++){
//				v[k][i] = checkValue(Math.log(markovModel.states[k].get(markovModel.sigma.indexOf(w.get(i-1))))) + checkValue(selectMax(k,i));
				v[k][i] = checkValue(Math.log(states.get(k).get(sigma.indexOf(w.get(i-1))))) + checkValue(selectMax(k,i));
			}
		}
		getAns2();
	}
	
	public void getAns(){
		double max = Double.NEGATIVE_INFINITY, tmp;
		int maxNum = 0;
		for(int i=0; i<markovModel.stnum-1; i++){
			tmp = checkValue(v[i][w.size()]) + Math.log(markovModel.deltas[i][markovModel.stnum-1]);
			if(tmp > max){
				max = tmp;
				maxNum = i;
			}
		}
		int nowStatus = markovModel.stnum-1;
		int i = w.size()+1;
		vp[markovModel.stnum-1][i] = maxNum;
		ans.add(markovModel.stnum-1);
		nowStatus = vp[markovModel.stnum-1][i];
		i--;
		while(i>0){
			ans.add(nowStatus);
			nowStatus = vp[nowStatus][i];
			i--;
		}
		ans.add(0);
		Collections.reverse(ans);
	}
	
	public void getAns2(){
		double max = Double.NEGATIVE_INFINITY, tmp;
		int maxNum = 0;
		for(int i=0; i<markovModel.stnum-1; i++){
			tmp = checkValue(v[i][w.size()]) + Math.log(st.deltas[i][markovModel.stnum-1]);
			if(tmp > max){
				max = tmp;
				maxNum = i;
			}
		}
		int nowStatus = markovModel.stnum-1;
		int i = w.size()+1;
		vp[markovModel.stnum-1][i] = maxNum;
		ans.add(markovModel.stnum-1);
		nowStatus = vp[markovModel.stnum-1][i];
		i--;
		while(i>0){
			ans.add(nowStatus);
			nowStatus = vp[nowStatus][i];
			i--;
		}
		ans.add(0);
		Collections.reverse(ans);
	}
	
	
	private double selectMax(int nowStatus, int nowTimes){
		double max = Double.NEGATIVE_INFINITY, tmp;
		int maxNum = 0;
		for(int i=0; i<markovModel.stnum-1; i++){
			tmp = checkValue(v[i][nowTimes-1]) + Math.log(markovModel.deltas[i][nowStatus]);
			if(tmp > max){
				max = tmp;
				maxNum = i;
			}
		}
		vp[nowStatus][nowTimes] = maxNum;
		return max;
	}
	
	private double checkValue(double a){
		if(a == Double.NEGATIVE_INFINITY){
			return 0;
		}else if( a == 0.0){
			return Double.NEGATIVE_INFINITY;
		}else{
			return a;
		}
	}
	
	public void resetInf(){
		ans.removeAll(ans);
		w.removeAll(w);
	}
	
	public void putStates(){
		for(int i=0; i<states.size(); i++){
			System.out.println("state " + i + ":" + states.get(i));
		}
	}
	
	public void setSigmas(){
		for(int i=0; i<markovModel.sigmaAns.size(); i++){
			if( sigma.indexOf(markovModel.sigmaAns.get(i)) == -1){
				//始めてみる記号列ならば…
				sigma.add(markovModel.sigmaAns.get(i));
			}
		}
		sigma = markovModel.sigma;
	}
	
	public boolean checkAns(){
		System.out.println("ans   :"+ans);
		System.out.println("oldans:"+oldans);
		for(int i=0; i<ans.size(); i++){
			if(ans.get(i) == oldans.get(i)){
				
			}else{
				return false;
			}
		}
		return true;
	}
	
	public void copyStates(){
		for(int i=0; i<markovModel.stnum; i++){
			for(int j=0; j<markovModel.sigma.size(); j++){
				st.states.get(i).set(j, states.get(i).get(j));
			}
		}
	}
}
