package markovmodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Study {
	protected Model markovModel;
	protected List<Integer> pass;
	protected HashMap<Integer, Integer> stateCounter; //状態indexが何回現れたかをカウント
	protected List<SigmaData> sigmaCounter; //状態index1でsigmaindex2が何回現れたかをカウント
	
	protected List<String> sigma;
	protected int stnum;
	public List<List<Double>> states;
	protected double[][] deltas;
	protected int[][] deltaCounter;
	
	public Study(Model m){
		markovModel = m;
		//this.inf = inf;
		stateCounter = new HashMap<Integer, Integer>();
		sigmaCounter = new ArrayList<SigmaData>(0);
		pass = new ArrayList<Integer>();
		sigma = new ArrayList<String>();
		stnum = markovModel.stnum;
		states = new ArrayList<List<Double>>();
		deltas = new double[stnum][stnum];
		deltaCounter = new int[stnum][stnum];
		
		for(int i=0; i<stnum; i++){
			for(int j=0; j<stnum; j++){
				deltas[i][j] = 0.0;
				deltaCounter[i][j] = 0;
			}
		}
		//initializeStates();
	}
	
	public void doStudy(int count, List<Integer> x){
		int statustmp = 0;
		int from, to;
		from = 0;
		to = -1;
		
		setPass(x);
		
		//記号の出現確率を求める
		for(int i=0; i<pass.size(); i++){
			statustmp = pass.get(i);
			if(!stateCounter.containsKey(statustmp)){
				//初めて見る状態ならば…
				stateCounter.put(statustmp, 1);
			}else if(stateCounter.containsKey(statustmp)){
				stateCounter.put(statustmp, stateCounter.get(statustmp)+1);
			}
			
			if(to == -1){
				
			}else{
				from = to;
			}
			to = statustmp;
			deltaCounter[from][to] += 1;
			
			
			if( sigma.indexOf(markovModel.sigmaAns.get(i)) == -1){
				//始めてみる記号列ならば…
				sigma.add(markovModel.sigmaAns.get(i));
				SigmaData stmp = new SigmaData(markovModel.sigmaAns.get(i), statustmp);
				sigmaCounter.add(stmp);
			}else{
				//既に見たことがあるものなら…
				int c=0;
				String str = markovModel.sigmaAns.get(i);
				for(int j=0; j<sigmaCounter.size(); j++){
					if(str.equals(sigmaCounter.get(j).sigma) && statustmp == sigmaCounter.get(j).status){
						sigmaCounter.get(j).count++;
						c = 1;
					}
				}
				
				if(c == 1){
					//何もしない
				}else{
					for(int j=0; j<sigmaCounter.size(); j++){
						if(str.equals(sigmaCounter.get(j).sigma) && statustmp != sigmaCounter.get(j).status){
							SigmaData stmp = new SigmaData(markovModel.sigmaAns.get(i), statustmp);
							sigmaCounter.add(stmp);
						}
					}
				}
				
			}
		}
		
		from = to;
		to = stnum-1;
		deltaCounter[from][to] += 1;
		
		
		for(int i=0; i<stnum; i++){
			if(stateCounter.get(i) == null){
				stateCounter.put(i, 0);
			}
		}
		
		//統計開始
		//各状態でのsigmaの出現確率
		List<Double> tmp = new ArrayList<Double>();
		for(int i=0; i<sigma.size(); i++){
			tmp.add(0.0);
		}
		states.add(tmp);
		for(int i=1; i<stnum-1; i++){
			int statusCount = stateCounter.get(i);
			tmp = new ArrayList<Double>();
			
			//j番目のsigmaの確率計算
			for(int j=0; j<sigma.size(); j++){
				for(int k=0; k<sigmaCounter.size(); k++){
					if(i == sigmaCounter.get(k).status){
						if(sigma.get(j).equals(sigmaCounter.get(k).sigma) && i == sigmaCounter.get(k).status){
							if(statusCount != 0)
								tmp.add((double)(sigmaCounter.get(k).count)/statusCount);
							else;
							//	tmp.add(0.0);
						}
					}
				}
				if(j+1 != tmp.size()){
					tmp.add(0.0);
				}
			}
			states.add(tmp);
		}
		
		tmp = new ArrayList<Double>();
		for(int i=0; i<sigma.size(); i++){
			tmp.add(0.0);
		}
		states.add(tmp);
		
		
		//deltaの確率計算
		for(int i=0; i<stnum; i++){
			for(int j=0; j<stnum; j++){
				if(deltaCounter[i][j] != 0){
					deltas[i][j] = deltaCounter[i][j]/(double)count;
				}else{
					deltas[i][j] = deltaCounter[i][j];
				}
				
				if( stateCounter.get(i) != 0){
					deltas[i][j] = (double)deltaCounter[i][j]/(double)(stateCounter.get(i));
				}
			}
		}
	}
	
	
	public void setPass(List<Integer> x){
		for(int i=1; i<x.size()-1; i++){
			pass.add(x.get(i));
		}
	}
	
	public void setSigmas(List<Integer> x){
		setPass(x);
		for(int i=0; i<pass.size(); i++){
			int statustmp = pass.get(i);
			if( sigma.indexOf(markovModel.sigmaAns.get(i)) == -1){
				//始めてみる記号列ならば…
				sigma.add(markovModel.sigmaAns.get(i));
				SigmaData stmp = new SigmaData(markovModel.sigmaAns.get(i), statustmp);
				sigmaCounter.add(stmp);
			}
		}
	}
	
	public void resetSt(){
		pass.removeAll(pass);
		states.removeAll(states);
		initializeStates();
	}
	
	public void resetCounter(){
		for(int i=0; i<stnum; i++){
			stateCounter.put(i,0);
		}
		
		for(int i=0; i<sigmaCounter.size(); i++){
			sigmaCounter.get(i).count=0;
		}
		
		for(int i=1; i<stnum; i++){
			for(int j=0; j<stnum; j++){
				deltaCounter[i][j] = 0;
			}
		}
		
	}
	
	
	public void putData(){
		putSigma();
		putStnum();
		putStatusCount();
		putSigmaCount();
		putdeltaCount();
	}
	
	public void putAns(){
		putStates();
		putDeltas();
	}
	
	public void putSigma(){
		System.out.print("sigma:");
		for(int i=0; i<sigma.size(); i++){
			System.out.print(sigma.get(i) + " ");
		}
		System.out.println();
	}
	
	private void putStnum(){
		System.out.println("stnum:" + stnum);
	}
	
	private void putStatusCount(){
		for(int i=0; i<stnum; i++){
			System.out.print("status:" + i);
			System.out.println(" " + stateCounter.get(i));
		}
	}
	
	private void putSigmaCount(){
		for(int i=0; i<sigmaCounter.size(); i++){
			System.out.print("status:" + sigmaCounter.get(i).status);
			System.out.print(" sigma:" + sigmaCounter.get(i).sigma );
			System.out.println(" " + sigmaCounter.get(i).count);
		}
	}
	
	private void putdeltaCount(){
		for(int i=0; i<stnum; i++){
			for(int j=0; j<stnum; j++){
				System.out.print("delta[" + i + "][" + j + "]:" + deltaCounter[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	public void putStates(){
		for(int i=0; i<states.size(); i++){
			System.out.print("state " + (i) + ":");
			System.out.println(states.get(i));		
			}
	}
	
	public void putDeltas(){
		for(int i=0; i<stnum; i++){
			for(int j=0; j<stnum; j++){
				System.out.println("delta " + i + " " + j + " : " + deltas[i][j]);
			}
		}
	}
	
	private void initializeStates(){
		List<Double> tmp = new ArrayList<Double>();;
		for(int i=0; i<stnum; i++){
			tmp = new ArrayList<Double>();
			for(int j=0; j<markovModel.sigma.size(); j++){
				tmp.add(0.0);
			}
			states.add(tmp);
		}
	}
	
	
	
}
