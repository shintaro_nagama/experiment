package markovmodel;

import java.io.IOException;


public class Main{
	static Model markovModel;
	static IO io;
	static DoModel dm;
	static Inference inf;
	static Study st;
	//static Experiment ex;
	
	public static void main(String[] args) throws IOException{
		markovModel = new Model();
		inf = new Inference(markovModel);
		io = new IO(markovModel, inf);
		
		io.loadFile();
		markovModel.initializeState2();
		markovModel.modelCheck();
		
		dm = new DoModel(markovModel, inf);
		dm.doModel(100);
		io.putSS();
		inf.initialize();
		inf.inference();
		io.putInference();
		
		/*
		st = new Study(markovModel);
		inf.setSt(st);
		inf.setRandomStates();
		inf.putStates();
		st.putDeltas();
		inf.copyStates();
		st.putStates();
		inf.setSigmas();
		inf.inference2();
		st.doStudy(1, inf.ans);
		System.out.println("=======================");
		st.putAns();
		inf.update();
		st.resetSt(); //passと確率を削除
		inf.copyStates();
		inf.ans.removeAll(inf.ans); //passを削除
		inf.inference2(); //更新した確率を用いて新しくpassを生成
		st.resetCounter(); //確率を出すためのカウントをリセット
		st.doStudy(2, inf.ans); 
		System.out.println("=======================");
		st.putAns();
		*/
		
		
		/*
		//課題３　
		st.doStudy(1, markovModel.statusAns);
		
		for(int i=0; i<10000; i++){
			reset();
			dm.doModel(100);
			st.doStudy(i+2, markovModel.statusAns);
		}
		*/

		
		
		//課題4
		/*
		for(int j=0; j<1; j++){
			reset();
			dm.doModel(100);
			io.putSigmaAns();
			int cnt = 0;
			int i = 1;
			inf.setRandomStates();
			inf.setSigmas();
			System.out.println("inf's w" + inf.w);
			inf.inference2(); 
			System.out.println("inf's pass:" + inf.ans);
			st.doStudy(i, inf.ans);
			//st.putAns();
			i++;
			while(i <= 100 && i != -1){
				System.out.println(j + " " + i);
				inf.update(); // 確率の更新
				System.out.println("st's ans");
				st.putAns();
				System.out.println("after update");
				inf.putStates();
				st.putDeltas();
				st.resetSt(); //passと確率を削除
				//st.pass.removeAll(st.pass);
				inf.ans.removeAll(inf.ans); //passを削除
				inf.inference2(); //更新した確率を用いて新しくpassを生成
				System.out.println("inf's pass:" + inf.ans);
				//st.resetCounter(); //確率を出すためのカウントをリセット
				st.doStudy(i, inf.ans); 
				//st.putAns();
				if(inf.checkAns()){
					//状態遷移が変わらなければ、終わる
					i=-1;
				}else{
					i++;
					cnt = 0;
				}
			}
			
		}
		*/
		/*
		System.out.println("学習結果");
		System.out.println(inf.ans);
		st.putSigma();
		st.putAns();
		*/
	}
	
	public static void reset(){
		markovModel.resetModel();
		st.resetSt();
		inf.resetInf();
	}
	
	private void setdata(){
		st.deltas[0][0] = 0.0;
		st.deltas[0][1] = 0.5;
		st.deltas[0][2] = 0.5;
		st.deltas[0][3] = 0.0;
		st.deltas[1][0] = 0.0;
		st.deltas[1][1] = 0.8;
		st.deltas[1][2] = 0.19;
		st.deltas[1][3] = 0.01;
		st.deltas[2][0] = 0.0;
		st.deltas[2][1] = 0.19;
		st.deltas[2][2] = 0.8;
		st.deltas[2][3] = 0.01;
		st.deltas[3][0] = 0.0;
		st.deltas[3][1] = 0.0;
		st.deltas[3][2] = 0.0;
		st.deltas[3][3] = 0.0;
		
		inf.states.get(0).set(0, 0.0);
		inf.states.get(0).set(1, 0.0);
		inf.states.get(0).set(2, 0.0);
		inf.states.get(0).set(3, 0.0);
		inf.states.get(0).set(4, 0.0);
		inf.states.get(0).set(5, 0.0);
		inf.states.get(1).set(0, 0.8);
		inf.states.get(1).set(1, 0.05);
		inf.states.get(1).set(2, 0.05);
		inf.states.get(1).set(3, 0.05);
		inf.states.get(1).set(4, 0.05);
		inf.states.get(1).set(5, 0.0);
		inf.states.get(2).set(0, 0.0);
		inf.states.get(2).set(1, 0.05);
		inf.states.get(2).set(2, 0.05);
		inf.states.get(2).set(3, 0.05);
		inf.states.get(2).set(4, 0.05);
		inf.states.get(2).set(5, 0.08);
		inf.states.get(3).set(0, 0.0);
		inf.states.get(3).set(1, 0.0);
		inf.states.get(3).set(2, 0.0);
		inf.states.get(3).set(3, 0.0);
		inf.states.get(3).set(4, 0.0);
		inf.states.get(3).set(5, 0.0);
	}
	
}
