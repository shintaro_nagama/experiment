package kadai1;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	static String filename;
	public static void main(String[] args) throws IOException{
		//ファイル名の入力
		filename = inputFilename();
		
		if(filename.equals("all")){
			filename = "euc.txt";
			System.out.println("------" + filename + "-----");
			outputHex(filename);
			filename = "jis.txt";
			System.out.println("------" + filename + "-----");
			outputHex(filename);
			filename = "sjis.txt";
			System.out.println("------" + filename + "-----");
			outputHex(filename);
			filename = "utf8.txt";
			System.out.println("------" + filename + "-----");
			outputHex(filename);
			filename = "utf16.txt";
			System.out.println("------" + filename + "-----");
			outputHex(filename);
		}else{
			//ファイルの読み込み
			System.out.println("------" + filename + "-----");
			outputHex(filename);
		}
	}
	
	//ファイル名の入力
	static public String inputFilename() throws IOException{
		InputStreamReader inputFile = new InputStreamReader(System.in);
		BufferedReader buf = new BufferedReader(inputFile);
		String str;
		System.out.println("読み込むファイル名を入力してください");
		str = buf.readLine();
		
		inputFile.close();
		buf.close();
		
		return str;
	}
	
	//ファイルを読み込んで1バイトごとに16進数で出力
	static public void outputHex(String filename) throws IOException{
		FileInputStream input = new FileInputStream(filename);
		int Byte;
		while((Byte = input.read()) != -1){
			System.out.print(Integer.toHexString(Byte) +" ");
		}
		System.out.println("");
		input.close();
	}
}
