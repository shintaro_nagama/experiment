package kadai3_2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;

public class Main {
	static List<Map<Character, Integer>> mapList = new ArrayList<Map<Character, Integer>>();
	static List<String> sentenceList = new ArrayList<String>();
	static Map<Character, Integer> chCounter = new HashMap<Character, Integer>();
	static boolean lineCheck = true, charCheck = true;
	static String str, bufString;
	static char ch;
	
	public static void main(String[] args) throws IOException{
		FileInputStream input = new FileInputStream("r2.txt");
		InputStreamReader isr = new InputStreamReader(input, "UTF-8");
		BufferedReader br = new BufferedReader(isr);
		
		// ファイルから１文字ずつ読み込み、バッファへ追加します。
		while((str = br.readLine()) != null){
			str = str.trim();
			sentenceList.add(str); //記事を追加
			Map<Character, Integer> maptmp = new HashMap<Character, Integer>();
			for(int i=0; i<str.length(); i++){
				if( Character.codePointAt(str, i)== 65279 ){
					//BOMを除外
					break;
				}
				ch = str.charAt(i); //N文字目
				Character.UnicodeBlock block = Character.UnicodeBlock.of(ch);
				if(block != Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS){
					//漢字じゃなければ何もしない
				}else{
					//漢字だった時の処理
					if(maptmp.containsKey(ch)){
						maptmp.put(ch, maptmp.get(ch)+1);
					}else{
						maptmp.put(ch, 1);
						if(chCounter.containsKey(ch)){
							//何もせず
						}else{
						chCounter.put(ch, 0);
						}
					}
				}
			}
			mapList.add(maptmp);
		}
		
		input.close();
		System.out.println(sentenceList.size());
		for(int i=0; i<sentenceList.size(); i++){
			str = sentenceList.get(i);
			Map<Character, Integer> maptmp = new HashMap<Character, Integer>();
			for(int j=0; j<str.length(); j++){
				if( Character.codePointAt(str, j)== 65279 ){
					//BOMを除外
					break;
				}
				ch = str.charAt(j); //N文字目
				Character.UnicodeBlock block = Character.UnicodeBlock.of(ch);
				if(block != Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS){
					//漢字じゃなければ何もしない
				}else{
					//漢字だった時の処理
					if(maptmp.containsKey(ch)){
						maptmp.put(ch, maptmp.get(ch)+1);
					}else{
						maptmp.put(ch, 1);
						chCounter.put(ch, chCounter.get(ch)+1);
					}
				}
			}
			mapList.add(maptmp);
		}
		
		for(int i=0; i<sentenceList.size(); i++){
			str = sentenceList.get(i);
			System.out.println(str);
			List<Entry<Character, Integer>> entries = new ArrayList<Entry<Character, Integer>>(mapList.get(i).entrySet());
			//Comparator で Map.Entry の値を比較
			Collections.sort(entries, new Comparator<Entry<Character, Integer>>() {
			    //比較関数
			    @Override
			    public int compare(Entry<Character, Integer> o1, Entry<Character, Integer> o2) {
			        //return o1.getValue().compareTo(o2.getValue());    //昇順
			        return o2.getValue().compareTo(o1.getValue());    //降順
			    }
			});
			/*
			//確認用
			for (Entry<Character, Integer> e : entries) {
			    System.out.println(e.getKey() + " = " + e.getValue());
			}
			*/
			double max = 0.0;
			double max2 = 0.0;
			char ch = 'a';
			char ch2 = 'a';
			for (Entry<Character, Integer> e : entries) {
			    //System.out.println(e.getKey() + " = " + e.getValue());
				//System.out.println(e.getValue() +"/"+ str.length());
				double tf = e.getValue()/(double)str.length();
				int df = chCounter.get(e.getKey());
				double idf = Math.log(sentenceList.size()/df);
				double tfIdf = tf*idf;
				if( max < tfIdf){
					max2 = max;
					max = tfIdf;
					ch2 = ch;
					ch = e.getKey();
					
				}else if(max2 < tfIdf){
					max2 = tfIdf;
					ch2 = e.getKey();
				}
			}
			System.out.println(ch + ":" +  max);
			System.out.println(ch2 + ":" +  max2);
			/*
			double tf1 = entries.get(0).getValue() / (double)str.length();
			double tf2 = entries.get(1).getValue() / (double)str.length();
			int df1 = chCounter.get(entries.get(0).getKey());
			int df2 = chCounter.get(entries.get(1).getKey());
			double idf1 = Math.log(sentenceList.size()/df1);
			double idf2 = Math.log(sentenceList.size()/df2);
			double tfIdf1 = tf1*idf1;
			double tfIdf2 = tf2*idf2;
			System.out.println(entries.get(0).getKey() + ":" + tfIdf1 );
			System.out.println(entries.get(1).getKey() + ":" + tfIdf2 );
			*/
		}
		
	}
}
