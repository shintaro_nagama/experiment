package kadai5_1;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;

import org.atilika.kuromoji.Token;
import org.atilika.kuromoji.Tokenizer;

public class Main {
	//static Map<List<String>, Map<String, Integer>> rootmap =
	//		  new HashMap<List<String>, Map<String, Integer>>();
	//static int N = 2;
	//static List<String> randomString = new ArrayList<String>();
	//static List<String> bufString = new ArrayList<String>();
	static List<String> strList = new ArrayList<String>();
	static List<List<String>> sentenceList = new ArrayList<List<String>>();//文章を保存
	static Map<String, Integer> wordCounter = new HashMap<String, Integer>();//それぞれの文章の単語数を保存
	static List<Map<String, Integer>> mapList = new ArrayList<Map<String, Integer>>(); //1つの文章で何回その単語が出てきたか
	static String str;
	static List<String> start = new ArrayList<String>();
	static StringBuilder strBuilder = new StringBuilder();
	static char ch;
	public static void main(String[] args) throws IOException{
		FileInputStream input = new FileInputStream("r2.txt");
		InputStreamReader isr = new InputStreamReader(input, "UTF-8");
		BufferedReader br = new BufferedReader(isr);
		
		//String patern;
		while((str = br.readLine()) != null){
			str = str.trim();
			strList = new ArrayList<String>();
			Map<String, Integer> maptmp = new HashMap<String, Integer>();
			if( Character.codePointAt(str, 0)== 65279 ){
				//BOMを除外
				break;
			}
			// 解析器オブジェクトを作成
		    Tokenizer tokenizer = Tokenizer.builder().build();
		    // 文を解析する
		    List<Token> tokens = tokenizer.tokenize(str);
		    
		    for (Token token : tokens) {
		    	String tmp =  token.getSurfaceForm();
		    	strList.add(tmp);
		    	List<String> hinshi = Arrays.asList(token.getAllFeaturesArray());
		    	//System.out.println(hinshi.get(0) + ":" + hinshi.get(1) + ":" + token.getSurfaceForm());
		    	if(hinshi.get(0).equals("名詞")){
		    		if(!hinshi.get(1).equals("代名詞") && !hinshi.get(1).equals("数") && !hinshi.get(1).equals("接尾") && !hinshi.get(1).equals("非自立") ){
		    			if(maptmp.containsKey(tmp)){
							int cnt = maptmp.get(tmp);
							maptmp.put(tmp, cnt+1);
							//System.out.println(hinshi.get(0) + ":" + hinshi.get(1) + ":" + tmp);
						}else{
							maptmp.put(tmp, 1);
							if(wordCounter.containsKey(tmp)){
								//既に登録されている
							}else{
								wordCounter.put(tmp, 0);
							}
							//System.out.println(hinshi.get(0) + ":" + hinshi.get(1) + ":" + tmp);
						}
		    		}
		    	}
		    }
		    sentenceList.add(strList);
		    mapList.add(maptmp);
		}
		input.close();
		
		for(int i=0; i<sentenceList.size(); i++){
			List<String> strtmp = sentenceList.get(i);
			Map<String, Integer> maptmp = new HashMap<String, Integer>();
			for(int j=0; j<sentenceList.get(i).size(); j++){
				if(mapList.get(i).containsKey(strtmp.get(j))){
					if(maptmp.containsKey(strtmp.get(j))){
						//文章で2回目以上だから無視
					}else{
						maptmp.put(strtmp.get(j), 1);
						int c = wordCounter.get(strtmp.get(j));
						wordCounter.put(strtmp.get(j), c+1);
					}
				}else{
					//シカト
				}
			}
		}
		/*
		for(int i=0; i<mapList.size(); i++){
			System.out.println("######################");
			System.out.println(mapList.get(i));
		}
		System.out.println("WordCounter######################");
		System.out.println(wordCounter);
		*/
		
		for(int i=0; i<sentenceList.size(); i++){
			strBuilder = new StringBuilder();
			for(String str : sentenceList.get(i)){
				strBuilder.append(str);
			}
			String s = strBuilder.toString();
			System.out.println(s);
			//System.out.println("######################");
			//System.out.println(mapList.get(i));
			List<Entry<String, Integer>> entries = new ArrayList<Entry<String, Integer>>(mapList.get(i).entrySet());
			//Comparator で Map.Entry の値を比較
			Collections.sort(entries, new Comparator<Entry<String, Integer>>() {
			    //比較関数
			    @Override
			    public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
			        //return o1.getValue().compareTo(o2.getValue());    //昇順
			        return o2.getValue().compareTo(o1.getValue());    //降順
			    }
			});
			Map<String, Double> ranking = new HashMap<String, Double>();
			/*
			double max = 0.0;
			double max2 = 0.0;
			String str1 = "";
			String str2 = "";
			*/
			for (Entry<String, Integer> e : entries) {
				//System.out.println(e.getKey() + " = " + e.getValue());
				//System.out.println(e.getValue() +"/"+ sentenceList.get(i).size());
				double tf = e.getValue()/(double)sentenceList.get(i).size();
				int df = wordCounter.get(e.getKey());
				double idf = Math.log(sentenceList.size()/(double)df);
				double tfIdf = tf*idf;
				ranking.put(e.getKey(), tfIdf);
			}
			
			List<Entry<String, Double>> entries2 = new ArrayList<Entry<String, Double>>(ranking.entrySet());
			Collections.sort(entries2, new Comparator<Entry<String, Double>>() {
			    //比較関数
			    @Override
			    public int compare(Entry<String, Double> o1, Entry<String, Double> o2) {
			        //return o1.getValue().compareTo(o2.getValue());    //昇順
			        return o2.getValue().compareTo(o1.getValue());    //降順
			    }
			});
			/*
			//確認用
			for (Entry<String, Double> e : entries2) {
			    System.out.println(e.getKey() + " : " + e.getValue());
			}*/
			for(int j=0; j<3; j++){
				System.out.println(entries2.get(j).getKey() + " : " + entries2.get(j).getValue());
			}
		}
	}
}
