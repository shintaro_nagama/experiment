package kadai4_1;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Map.Entry;

import org.atilika.kuromoji.Token;
import org.atilika.kuromoji.Tokenizer;

public class Main {
	static HashMap<String, Integer> hashmap = new HashMap<String,Integer>();
	static int charNum; //文字数
	
	static public void main(String[] args) throws IOException{
		FileInputStream input = new FileInputStream("r2.txt");
		InputStreamReader isr = new InputStreamReader(input, "UTF-8");
		BufferedReader br = new BufferedReader(isr);
		
		String str;
		// ファイルから１文字ずつ読み込み、バッファへ追加します。
		while ((str = br.readLine()) != null) {
			// 解析器オブジェクトを作成
		    Tokenizer tokenizer = Tokenizer.builder().build();
		    // 文を解析する
		    List<Token> tokens = tokenizer.tokenize(str);
		    
		    // 結果を出力してみる
		    for (Token token : tokens) {
		    	List<String> hinshi = Arrays.asList(token.getAllFeaturesArray());
		    	//System.out.println(hinshi.get(0) + ":" + hinshi.get(1) + ":" + token.getSurfaceForm());
		    	if(hinshi.get(0).equals("名詞")){
		    		if(!hinshi.get(1).equals("代名詞") && !hinshi.get(1).equals("数") && !hinshi.get(1).equals("接尾") && !hinshi.get(1).equals("非自立") ){
		    			String tmp =  token.getSurfaceForm();
		    			if(hashmap.containsKey(tmp)){
							int cnt = hashmap.get(tmp);
							hashmap.put(tmp, cnt+1);
							//System.out.println(hinshi.get(0) + ":" + hinshi.get(1) + ":" + tmp);
						}else{
							hashmap.put(tmp, 1);
							//System.out.println(hinshi.get(0) + ":" + hinshi.get(1) + ":" + tmp);
						}
		    		}
		    	}
		    }
		}
		input.close();
		
		/*****
		mapの値をソート
		*****/
		List<Entry<String, Integer>> entries = new ArrayList<Entry<String, Integer>>(hashmap.entrySet());

		//Comparator で Map.Entry の値を比較
		Collections.sort(entries, new Comparator<Entry<String, Integer>>() {
		    //比較関数
		    @Override
		    public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
		        //return o1.getValue().compareTo(o2.getValue());    //昇順
		        return o2.getValue().compareTo(o1.getValue());    //降順
		    }
		});
		
		/*
		//確認用
		for (Entry<String, Integer> e : entries) {
		    System.out.println(e.getKey() + " = " + e.getValue());
		}
		*/
		System.out.println(hashmap.size());
		System.out.println(entries.get(0).getValue() + "("+ entries.get(0).getKey() +")");
		System.out.println(entries.get(1).getValue() + "("+ entries.get(1).getKey() +")");
		System.out.println(entries.get(2).getValue() + "("+ entries.get(2).getKey() +")");
	}
	
}
