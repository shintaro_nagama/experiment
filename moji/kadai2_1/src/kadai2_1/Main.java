package kadai2_1;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class Main {

	public static void main(String[] args) throws IOException{
		FileInputStream input = new FileInputStream("r2_1.txt");
		InputStreamReader isr = new InputStreamReader(input, "UTF-8");
		BufferedReader br = new BufferedReader(isr);
		
		String str;
		// ファイルから１文字ずつ読み込み、バッファへ追加します。
		while ((str = br.readLine()) != null) {
			//System.out.println(str);
			char c = str.charAt(0);
			Character.UnicodeBlock block = Character.UnicodeBlock.of(c);
			if(block == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS){
				//漢字の場合
				System.out.println(c + ":Kanji");
			}else if(block == Character.UnicodeBlock.HIRAGANA){
				//ひらがなの場合
				System.out.println(c + ":Hiragana");
			}else if(block == Character.UnicodeBlock.KATAKANA){
				//カタカナの場合
				System.out.println(c + ":Katakana");
			}else if(block == Character.UnicodeBlock.BASIC_LATIN || block == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS){
				//ASCIIの場合
				if(Character.isLetter(c)){
					//文字ならば
					System.out.println(c + ":Alphabet");
				}else if(Character.isDigit(c)){
					//数字ならば
					System.out.println(c + ":Number");
				}else{
					System.out.println(c + ":Other");
				}
				
			}else{
				//その他の場合
				System.out.println(c + ":Other");
			}
		}
	}
}
