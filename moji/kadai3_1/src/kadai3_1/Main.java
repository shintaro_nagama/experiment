package kadai3_1;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;
import java.util.Set;

public class Main {
	static Map<String, Map<Character, Integer>> hashmap = new HashMap<String, Map<Character, Integer>>();
	static int N = 6;
	static boolean lineCheck = true, charCheck = true;
	static String str, bufString;
	static StringBuilder randomString = new StringBuilder();
	static char ch;
	public static void main(String[] args) throws IOException{
		FileInputStream input = new FileInputStream("r2.txt");
		InputStreamReader isr = new InputStreamReader(input, "UTF-8");
		BufferedReader br = new BufferedReader(isr);
		
		String patern;
		// ファイルから１文字ずつ読み込み、バッファへ追加します。
		str = br.readLine();
		
		for(int i=0; i<str.length(); i++){
			if( Character.codePointAt(str, i)== 65279 ){
				//BOMを除外
				break;
			}
			if( i == str.length()-N ){
				//末尾にいくまえに文字列を追加
				String test;
				if( (test = br.readLine()) != null){
					System.out.print("追加:");
					System.out.println(test);
					str = str.concat( test );
					str = str.trim();
				}else if((test = br.readLine()) == null){
					//最終行を読み込み終えたことを示す
					System.out.println("this line is last");
					lineCheck = false;
				}
			}
			
			if(i == str.length()-N-1 && lineCheck == false){
				System.out.println("this is last");
				patern = str.substring(i, i+(N-1));
				ch = str.charAt(i+N-1);
			}else{
				patern = str.substring(i, i+(N-1));
				ch = str.charAt(i+N-1); //N文字目
			}
			/*
			if(i == str.length()-N-1 && lineCheck == false){
				System.out.println("this is last");
			}
			patern = str.substring(i, i+(N-1));
			ch = str.charAt(i+N); //N文字目
			*/
			
			//System.out.println("i:" + i + "str.length()-N:" + (str.length()-N) + ":"+patern + "+" + ch);
			patern = patern.trim();
			if(hashmap.containsKey(patern)){
				if(hashmap.get(patern).containsKey(ch)){
					int cnt = hashmap.get(patern).get(ch);
					hashmap.get(patern).put(ch, cnt+1);
				}else{
					hashmap.get(patern).put(ch, 1);
				}
			}else{
				Map<Character, Integer> tmp = new HashMap<Character, Integer>();
				tmp.put(ch, 1);
				hashmap.put(patern, tmp);
			}
			
			if(lineCheck == false && i == str.length()-N){
				charCheck = false;
			}
			if(lineCheck==false && charCheck==false){
				break;
			}
		}
		input.close();

		List<Entry<String, Map<Character, Integer>>> entries = new ArrayList<Entry<String, Map<Character, Integer>>>(hashmap.entrySet());
		/*
		//確認用
		for (Entry<String, Map<Character, Integer>> e : entries) {
			//System.out.println(e.getKey() + "+" + e.getValue());
		}
		*/
		
		//文字列生成
		Random rnd = new Random();
		int sumtmp = 0;
		
		for(int i=0; i<1000; i++){
			bufString = str.substring(i, i+N-1);
			if(i==0){
				randomString.append(bufString);
			}
			sumtmp = 0;
			int tmp = 0;
			List<Entry<Character, Integer>> entries2 = new ArrayList<Entry<Character, Integer>>(hashmap.get(bufString).entrySet());			
			//Comparator で Map.Entry の値を比較
			Collections.sort(entries2, new Comparator<Entry<Character, Integer>>() {
			    //比較関数
			    @Override
			    public int compare(Entry<Character, Integer> o1, Entry<Character, Integer> o2) {
			        return o1.getValue().compareTo(o2.getValue());    //昇順
			        //return o2.getValue().compareTo(o1.getValue());    //降順
			    }
			});
			for (Entry<Character, Integer> e : entries2) {
				sumtmp += e.getValue();
			}
			int rand = rnd.nextInt(sumtmp);
			
			for (Entry<Character, Integer> e : entries2) {
				tmp += e.getValue();
			    if(rand < tmp ){
			    	char[] a = Character.toChars(e.getKey());
			    	randomString.append(a);
			    	//System.out.print(a);
			    	break;
			    }
			}
			if(randomString.length()%100 == 0){
				randomString.append(System.getProperty("line.separator"));
			}
		}
		String s = randomString.toString();
		System.out.println(s);
		
	}
}
