package kadai2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;

public class Main {
	static HashMap<Integer, Integer> hashmap = new HashMap<Integer,Integer>();
	static Integer maxKey, minKey;

	public static void main(String[] args) throws IOException{
		FileInputStream input = new FileInputStream("r1_2.txt");
		InputStreamReader isr = new InputStreamReader(input, "UTF-16LE");
		BufferedReader br = new BufferedReader(isr);
		
		String str;
		// ファイルから１文字ずつ読み込み、バッファへ追加します。
		while ((str = br.readLine()) != null) {
			System.out.println(str);
			for(int i=0; i<str.length(); i++){
				int codepoint = Character.codePointAt(str,i);
				if(codepoint == 65279){
					//BOMを除外
					break;
				}
				if(hashmap.containsKey(codepoint)){
					int cnt = hashmap.get(codepoint);
					hashmap.put(codepoint, cnt+1);
				}else{
					hashmap.put(codepoint, 1);
				}
			}
		}
		
		/*
		int Byte2, Byte1;
		String codepoint, str1, str2;
		while((Byte2 = input.read()) != -1){
			Byte1 = input.read();
			str2 = String.valueOf(Byte2);
			str1 = String.valueOf(Byte1);
			codepoint = str1 + str2;
			
			if(hashmap.containsKey(codepoint)){
				int cnt = hashmap.get(codepoint);
				hashmap.put(codepoint, cnt+1);
			}else{
				hashmap.put(codepoint, 1);
			}
		}
		*/
		input.close();
		
		int max, only1;
		max = 0;
		only1 = 0;
		for(Iterator<Integer> iterator = hashmap.keySet().iterator(); iterator.hasNext(); ) {
			Integer key = iterator.next();
			if(hashmap.get(key) > max){
				max = hashmap.get(key);
				maxKey = key;
			}
			if(hashmap.get(key) == 1){
				only1+=1;
			}
		}
		char[] maxc = Character.toChars(maxKey);
		
		System.out.print(maxc);
		System.out.println(":" + hashmap.get(maxKey));
		System.out.println(only1);
		
	}

}
