import java.util.Arrays;
import java.util.List;

import org.atilika.kuromoji.Token;
import org.atilika.kuromoji.Tokenizer;

public class LearnKuromoji0 {
  public static void main(String[] args) {
    // 解析器オブジェクトを作成
    Tokenizer tokenizer = Tokenizer.builder().build();

    // 文を解析する
    List<Token> tokens = tokenizer.tokenize("明日は月曜日です。");

    // 結果を出力してみる
    for (Token token : tokens) {
        System.out.println("==================================================");
        System.out.println("allFeatures : " + token.getAllFeatures());
        System.out.println("partOfSpeech : " + token.getPartOfSpeech());
        System.out.println("position : " + token.getPosition());
        System.out.println("reading : " + token.getReading());
        System.out.println("surfaceForm : " + token.getSurfaceForm());
        System.out.println("allFeaturesArray : " + Arrays.asList(token.getAllFeaturesArray())); // 配列はリストに変換すると簡単に印刷できる(豆知識)
        System.out.println("辞書にある言葉? : " + token.isKnown());
        System.out.println("未知語? : " + token.isUnknown());
        System.out.println("ユーザ定義? : " + token.isUser());
    }
  }
}
