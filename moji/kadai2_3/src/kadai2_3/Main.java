package kadai2_3;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Main {
	static Map<String, Integer> hashmap = new HashMap<String, Integer>();
	static int N = 6;
	static boolean lineCheck = true, charCheck = true;
	static String str;
	public static void main(String[] args) throws IOException{
		FileInputStream input = new FileInputStream("r2.txt");
		InputStreamReader isr = new InputStreamReader(input, "UTF-8");
		BufferedReader br = new BufferedReader(isr);
		
		String patern;
		// ファイルから１文字ずつ読み込み、バッファへ追加します。
		str = br.readLine();
		//System.out.println(str);
		//str = str.trim(); //先頭、末尾の空白を削除
		for(int i=0; i<str.length(); i++){
			if( Character.codePointAt(str, i)== 65279 ){
				//BOMを除外
				break;
			}
			if( i == str.length()-N){
				//末尾にいくまえに文字列を追加
				String test;
				if( (test = br.readLine()) != null){
					System.out.print("追加:");
					System.out.println(test);
					str = str.concat( test );
					str = str.trim();
					//System.out.println(str);
				}else if((test = br.readLine()) == null){
					//最終行を読み込み終えたことを示す
					System.out.println("this line is last");
					lineCheck = false;
				}
			}
			
			if(i == str.length()-N && lineCheck == false){
				System.out.println("this is last");
				patern = str.substring(str.length()-N);
			}else{
				patern = str.substring(i, i+N);
			}
			//System.out.println("i:" + i + "str.length()-N:" + (str.length()-N) + ":"+patern);
			patern = patern.trim();
			if(hashmap.containsKey(patern)){
				int cnt = hashmap.get(patern);
				hashmap.put(patern, cnt+1);
			}else{
				hashmap.put(patern, 1);
			}
			
			if(lineCheck == false && i == str.length()-N){
				charCheck = false;
			}
			if(lineCheck==false && charCheck==false){
				break;
			}
		}
		input.close();
		List<Entry<String, Integer>> entries = new ArrayList<Entry<String, Integer>>(hashmap.entrySet());

		//Comparator で Map.Entry の値を比較
		Collections.sort(entries, new Comparator<Entry<String, Integer>>() {
		    //比較関数
		    @Override
		    public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
		        return o1.getValue().compareTo(o2.getValue());    //昇順
		        //return o2.getValue().compareTo(o1.getValue());    //降順
		    }
		});
		
		//確認用
		for (Entry<String, Integer> e : entries) {
		    System.out.println(e.getKey() + " = " + e.getValue());
		}
		
		
		System.out.println(entries.get(entries.size()-1).getKey() + ":" + entries.get(entries.size()-1).getValue());
	}
}
