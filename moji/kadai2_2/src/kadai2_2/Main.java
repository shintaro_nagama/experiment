package kadai2_2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

public class Main {
	static HashMap<Integer, Integer> hashmap = new HashMap<Integer,Integer>();
	static int charNum; //文字数
	
	public static void main(String[] args) throws IOException{
		FileInputStream input = new FileInputStream("r2.txt");
		InputStreamReader isr = new InputStreamReader(input, "UTF-8");
		BufferedReader br = new BufferedReader(isr);
		
		String str;
		// ファイルから１文字ずつ読み込み、バッファへ追加します。
		while ((str = br.readLine()) != null) {
			//System.out.println(str);
			for(int i=0; i<str.length(); i++){
				int codepoint = Character.codePointAt(str,i);
				if(codepoint == 65279){
					//BOMを除外
					break;
				}
				if(hashmap.containsKey(codepoint)){
					int cnt = hashmap.get(codepoint);
					hashmap.put(codepoint, cnt+1);
				}else{
					hashmap.put(codepoint, 1);
				}
			}
		}
		input.close();
		/*****
		mapの値をソート
		*****/
		List<Entry<Integer, Integer>> entries = new ArrayList<Entry<Integer, Integer>>(hashmap.entrySet());

		//Comparator で Map.Entry の値を比較
		Collections.sort(entries, new Comparator<Entry<Integer, Integer>>() {
		    //比較関数
		    @Override
		    public int compare(Entry<Integer, Integer> o1, Entry<Integer, Integer> o2) {
		        return o1.getValue().compareTo(o2.getValue());    //昇順
		        //return o2.getValue().compareTo(o1.getValue());    //降順
		    }
		});
		
		//確認用
		for (Entry<Integer, Integer> e : entries) {
		    System.out.println(e.getKey() + " = " + e.getValue());
		}
		
		charNum = 0;
		for (Entry<Integer, Integer> e : entries) {
			charNum += e.getValue();
		}
		System.out.println("Number of char = " + charNum);
		Random rnd = new Random();
		int cnt = 0;
		for(int i=0; i<1000; i++){
			int rand = rnd.nextInt(charNum);
			int tmp = 0;
			for (Entry<Integer, Integer> e : entries) {
				tmp += e.getValue();
			    if(rand < tmp ){
			    	char[] a = Character.toChars(e.getKey());
			    	System.out.print(a);
			    	cnt++;
			    	break;
			    }
			}
			if(cnt >= 100){
				System.out.println("");
				cnt = 0;
			}	
		}
	}
	
}
