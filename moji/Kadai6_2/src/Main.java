import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.atilika.kuromoji.Token;
import org.atilika.kuromoji.Tokenizer;


public class Main {
		static List<String> strList = new ArrayList<String>(); 
		static List<Set<String>> cluster = new ArrayList<Set<String>>();
		static Map<Set<Set<String>>, Double> distanceMap = new HashMap<Set<Set<String>>, Double>();
		static Map<Set<String>, Integer> pairCounter = new HashMap<Set<String>, Integer>();
		static Map<Set<String>, Integer> pairCounter2 = new HashMap<Set<String>, Integer>(); //共起回数を数える
		static List<List<String>> sentenceList = new ArrayList<List<String>>();//文章を保存
		static Map<String, Integer> wordCounter = new HashMap<String, Integer>();//それぞれの文章の単語数を保存
		static List<Map<String, Integer>> mapList = new ArrayList<Map<String, Integer>>(); //1つの文章で何回その単語が出てきたか
		static List<Map<String, Double>> vector = new ArrayList<Map<String,Double>>();
		static List<List<String>> wordList = new ArrayList<List<String>>();
		static String str;
		static List<String> start = new ArrayList<String>();
		static StringBuilder strBuilder = new StringBuilder();
		static char ch;
		
		public static void main(String[] args) throws IOException{
			FileInputStream input = new FileInputStream("r2.txt");
			InputStreamReader isr = new InputStreamReader(input, "UTF-8");
			BufferedReader br = new BufferedReader(isr);
			

			while((str = br.readLine()) != null){
				str = str.trim();
				strList = new ArrayList<String>();
				Map<String, Integer> maptmp = new HashMap<String, Integer>();
				Set<String> strtmp = new HashSet<String>();
				if( Character.codePointAt(str, 0)== 65279 ){
					//BOMを除外
					break;
				}
				// 解析器オブジェクトを作成
			    Tokenizer tokenizer = Tokenizer.builder().build();
			    // 文を解析する
			    List<Token> tokens = tokenizer.tokenize(str);
			   
			    for (Token token : tokens) {
			    	String tmp =  token.getSurfaceForm();
			    	strList.add(tmp);
			    	List<String> hinshi = Arrays.asList(token.getAllFeaturesArray());
			    	if(hinshi.get(0).equals("名詞")){
			    		if(!hinshi.get(1).equals("代名詞") && !hinshi.get(1).equals("数") && !hinshi.get(1).equals("接尾") && !hinshi.get(1).equals("非自立") ){
			    			if(wordCounter.containsKey(tmp) && !strtmp.contains(tmp)){
								//既に登録されている
			    				int cnt = wordCounter.get(tmp);
			    				wordCounter.put(tmp, cnt+1);
							}else if(!wordCounter.containsKey(tmp) && !strtmp.contains(tmp)){
								wordCounter.put(tmp, 1);
							}
			    			strtmp.add(tmp);
			    		}
			    	}
			    }
			    sentenceList.add(strList);
				List<String> words = new ArrayList<String>();
				for(String s : strtmp){
					words.add(s);
				}
				//その文章で出てきた単語リスト
				wordList.add(words);
				
				//ペアを作ってカウント
	    		for(int i=0; i<words.size(); i++){
	    			for(int j=i+1; j<words.size(); j++){
	    				Set<String> settmp = new HashSet<String>();
		    			if(words.get(i)!=null && words.get(i).length()>0){
		    				settmp.add(words.get(i));
		    			}
	    				if(words.get(j)!=null && words.get(j).length()>0){
	    					settmp.add(words.get(j));
		    			}
	    				if(pairCounter.containsKey(settmp)){
	    					int cnt = pairCounter.get(settmp);
		    				pairCounter.put(settmp, cnt+1);
		    			}else{
		    				pairCounter.put(settmp, 1);
		    			}
	    			}
	    		}
			    System.out.println("======================================"+sentenceList.size()+"======================");
			}
			input.close();
			
			//文章に出てくる回数が6以下の単語を除く
			List<Entry<Set<String>, Integer>> wcounter = new ArrayList<Entry<Set<String>, Integer>>(pairCounter.entrySet());
			List<Set<String>> delWords = new ArrayList<Set<String>>(); //削除するペア
			for(Entry<Set<String>, Integer> e : wcounter){
				List<String> deltmp = new ArrayList<String>();
				for(String w : e.getKey()){
					deltmp.add(w);
				}
				if(wordCounter.get(deltmp.get(0)) < 6 || wordCounter.get(deltmp.get(1)) < 6){
					delWords.add(e.getKey());
				}
			}
			for(Set<String> del : delWords){
				pairCounter.remove(del);
			}
			/*
			
			
			
			double min = 1000;
			int distance_zero_cnt = 0;
			String word1, word2;
			word1 = "";
			word2 = "";
			List<Entry<Set<String>, Integer>> entries = new ArrayList<Entry<Set<String>, Integer>>(pairCounter.entrySet());
			for(Entry<Set<String>, Integer> e : entries){
				int[] cnt = new int[2];
				String[] words = new String[2];
				int s=0;
				for(String value : e.getKey()){
					cnt[s] = wordCounter.get(value);
					words[s] = value;
					s++;
				}
				if(words[0]!=null && words[0].length()>0&&words[1]!=null && words[1].length()>0){
					//double kyori = 1-e.getValue()*2/(wordCounter.get(e2.getKey())+wordCounter.get(e2.getValue()) );
					double kyori = (double)1-e.getValue()*2.0/(double)(cnt[0]+cnt[1]);
					if(kyori == 0.0){
						distance_zero_cnt++;
					}
					//System.out.println(words[0] + " " + words[1] + " " + cnt[0] + " " + cnt[1] + " " + pairCounter.get(e.getKey()) + " " + kyori);
					Set<String> tmp1 = new HashSet<String>();
					Set<String> tmp2 = new HashSet<String>();
					tmp1.add(words[0]);
					tmp2.add(words[1]);
					Set<Set<String>> setset = new HashSet<Set<String>>();
					setset.add(tmp1);
					setset.add(tmp2);
					distanceMap.put(setset, kyori);
				}
			}
			
			List<Entry<Set<Set<String>>, Double>> distances = new ArrayList<Entry<Set<Set<String>>, Double>>(distanceMap.entrySet());
			
			
			int z=0;
			while(true){
				min = 99999;
				List<List<String>> words = new ArrayList<List<String>>();
				Set<String> settmp1 = new  HashSet<String>();
				Set<String> settmp2 = new  HashSet<String>();
				distances = new ArrayList<Entry<Set<Set<String>>, Double>>(distanceMap.entrySet());
				Set<Set<String>> x = new HashSet<Set<String>>();
				for(Entry<Set<Set<String>>, Double> e : distances){
					double kyori = e.getValue();
					words.remove(words);
					for(Set<String> w : e.getKey()){
						List<String> a = new ArrayList<String>();
						a.addAll(w);
						words.add(a);
					}
					if(min > kyori && words.get(0).size()>0&&words.get(1).size()>0){
						min = kyori;
						settmp1.clear();
						settmp2.clear();
						words = new ArrayList<List<String>>();
						for(Set<String> tmp : e.getKey()){
							List<String> t = new ArrayList<String>();
							for(String a: tmp){
								t.add(a);
							}
							words.add(t);
						}
						settmp1.addAll(words.get(0));
						settmp2.addAll(words.get(1));
					}
				}
				x.add(settmp1);
				x.add(settmp2);
				//グループ化できる単語をリストから消す
				Set<String> clst = new HashSet<String>();
				
				for(String a : words.get(0)){
					clst.add(a);
					wordCounter.remove(a);
				}
				for(String a : words.get(1)){
					clst.add(a);
					wordCounter.remove(a);
				}
				
				//できたクラスタとそれぞれの単語との距離を計算
				for(Entry<Set<Set<String>>, Double> e : distances){
					//e.getKey()=集合の集合
					Set<Set<String>> tmp = new HashSet<Set<String>>();
					Set<String> set1 = new HashSet<String>();
					Set<String> set2 = new HashSet<String>();
					double max = 0;
					for(Set<String> settmp : e.getKey()){
						//set1に左側の集合
						//set2に右側の集合
						if(set1.size()<1){
							set1.addAll(settmp);
						}else if(set2.size()<1){
							set2.addAll(settmp);
						}
					}
					if(set1.size()==1 && set2.size()==1){
						String s1, s2;
						s1 = "";
						s2 = "";
						for(String s: set1){
							s1 = s;
						}
						for(String s: set2){
							s2 = s;
						}
						if(clst.contains(s1)){
							Set<String> st1 = new HashSet<String>();
							Set<String> st2 = new HashSet<String>();
							Set<Set<String>> setset = new HashSet<Set<String>>();
							Set<Set<String>> addset = new HashSet<Set<String>>();
							for(String s: clst){
								st1.clear();
								st2.clear();
								setset.clear();
								st1.add(s);
								st2.add(s2);	
								setset.add(st1);
								setset.add(st2);
								if(distanceMap.containsKey(setset)){
									double kyori = distanceMap.get(setset);
									if(max < kyori){
										max = kyori;
										addset.clear();
										addset.add(st1);
										addset.add(st2);
									}
									distanceMap.remove(setset);
								}
								distanceMap.remove(setset);
							}
							distanceMap.put(addset, max);
						}
						max = 0.0;
						if(clst.contains(s2)){
							Set<String> st1 = new HashSet<String>();
							Set<String> st2 = new HashSet<String>();
							Set<Set<String>> setset = new HashSet<Set<String>>();
							Set<Set<String>> addset = new HashSet<Set<String>>();
							for(String s: clst){
								st1.clear();
								st2.clear();
								setset.clear();
								st1.add(s);
								st2.add(s2);	
								setset.add(st1);
								setset.add(st2);
								if(distanceMap.containsKey(setset)){
									double kyori = distanceMap.get(setset);
									if(max < kyori){
										max = kyori;
										addset.clear();
										addset.add(st1);
										addset.add(st2);
									}
									distanceMap.remove(setset);
								}
								
							}
							distanceMap.put(addset, max);
						}
					}
					
				}
				
				if(distanceMap.size()==42000){
					for(Entry<Set<Set<String>>, Double> e : distances){
						System.out.println(e.getKey() + ":" + e.getValue());
					}
					break;
				}
				System.out.println("============="+distanceMap.size()+"============"+cluster.size());
			}
			for(int i=0; i<cluster.size(); i++){
				System.out.println(cluster);
			}
			*/
				
		}
		
		static public void putSentence(int n){
			strBuilder = new StringBuilder();
			for(String str : sentenceList.get(n)){
				strBuilder.append(str);
			}
			String s = strBuilder.toString();
			System.out.println(s);
		}
		
		static public void putPairs(){
			List<Entry<Set<String>, Integer>> pairs = new ArrayList<Entry<Set<String>, Integer>>(pairCounter.entrySet());
			for(Entry<Set<String>, Integer> e : pairs){
				System.out.print(e.getKey());
				System.out.println(e.getValue());
			}
		}
}
