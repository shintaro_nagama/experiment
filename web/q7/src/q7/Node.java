package q7;

import java.util.ArrayList;
import java.util.List;

public class Node {
	public List<Integer> s_predicates;
	public List<Integer> s_objects;
	public List<Integer> s_poequals;
	public List<Integer> p_subjects;
	public List<Integer> p_objects;
	public List<Integer> p_soequals;
	public List<Integer> o_subjects;
	public List<Integer> o_predicates;
	public List<Integer> o_spequals;
	public List<Integer> superclasses;
	public List<Integer> types;
	public List<Integer> domain;
	public List<Integer> range;
	public List<Integer> s_domain;
	public List<Integer> s_range;
	public List<Integer> subclasses;
	public List<Integer> instances;
	public List<Integer> superproperties;
	public List<Integer> subproperties;
	
	
	public Node(){
		s_predicates = new ArrayList<Integer>();
		s_objects = new ArrayList<Integer>();
		s_poequals = new ArrayList<Integer>();
		p_subjects = new ArrayList<Integer>();
		p_objects = new ArrayList<Integer>();
		p_soequals = new ArrayList<Integer>();
		o_subjects = new ArrayList<Integer>();
		o_predicates = new ArrayList<Integer>();
		o_spequals = new ArrayList<Integer>();
		superclasses = new ArrayList<Integer>();
		types = new ArrayList<Integer>();
		domain = new ArrayList<Integer>();
		range = new ArrayList<Integer>();
		s_domain = new ArrayList<Integer>();
		s_range = new ArrayList<Integer>();
		subclasses = new ArrayList<Integer>();
		instances = new ArrayList<Integer>();
		superproperties = new ArrayList<Integer>();
		subproperties = new ArrayList<Integer>();
	}
	
	//以下セッター
	public void s_addChild(int p, int o){
		s_predicates.add(p);
		s_objects.add(o);
	}
	
	public void s_addChild(int p){
		s_poequals.add(p);
	}
	
	public void p_addChild(int s, int o){
		p_subjects.add(s);
		p_objects.add(o);
	}
	
	public void p_addChild(int s){
		p_soequals.add(s);
	}
	
	public void o_addChild(int s, int p){
		o_subjects.add(s);
		o_predicates.add(p);
	}
	
	public void o_addChild(int s){
		o_spequals.add(s);
	}
	
	public void add_superClass(int o){
		superclasses.add(o);
	}
	
	public void add_subClass(int s){
		subclasses.add(s);
	}
	
	public void add_superProperty(int o){
		superproperties.add(o);
	}
	
	public void add_subProperty(int s){
		subproperties.add(s);
	}
	
	public void add_type(int o){
		types.add(o);
	}
	
	public void add_instance(int s){
		instances.add(s);
	}
	
	public void add_domain(int o){
		domain.add(o);
	}
	
	public void add_s_domain(int s){
		s_domain.add(s);
	}
	
	public void add_range(int o){
		range.add(o);
	}
	
	public void add_s_range(int s){
		s_range.add(s);
	}

}
