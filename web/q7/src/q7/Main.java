package q7;

import java.io.IOException;

public class Main {
	static makeNode nodeList = new makeNode();
	static Query query = new Query();
	public static void main(String args[]) throws IOException{
		nodeList.loadCSV();
		//nodeList.putNodes();
		query.loadQuery();
		query.putQuery();
		query.putVar();
		query.solvesQuery();
		query.putSubs();
	}
}
