package markov2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import markov2.Model;

public class IO {
	
	Model markovModel;
	public IO(Model model){
		this.markovModel = model;
	}
	public void loadFile() throws NumberFormatException, IOException{
		//ファイル名を入力し、CSVファイルをロードする
		//読み込むファイルはプロジェクトの真下に入れておく
		String fileName = "markovdata2.csv";
		
		try{
			//ファイルを読み込む
			FileReader fr = new FileReader(fileName);
			BufferedReader brCSV = new BufferedReader(fr);
			
			//読み込んだファイルを1行ずつ処理する
			String line;
			StringTokenizer token;
			List<Double> tmp = new ArrayList<Double>();
			int status = -1;	//sgima, delta,　など、何を入力している行なのかを判別するために用いる
			int stateNum, deltaNum1, deltaNum2; 		//state, deltaでどの状態かを保存するための変数
			stateNum = deltaNum1 = deltaNum2 = -1;
			while((line = brCSV.readLine()) != null){
				//区切り文字" "で分割する
				token = new StringTokenizer(line, " ");
				while(token.hasMoreTokens()){
					String word = token.nextToken();
					switch(word){
						case "sigma":{
							status = 0;
							continue;
						}
						case "stnum":{
							status = 1;
							continue;
						}
						case "state":{
							status = 2;
							stateNum = -1;
							tmp = new ArrayList<Double>();
							continue;
						}
						case "delta":{
							status = 3;
							deltaNum1 = -1;
							deltaNum2 = -1;
							continue;
						}
					}
					
					switch(status){
						case 0:{
							try{
							    markovModel.sigma.add(word);
							}catch(NumberFormatException e){
							    System.out.println("数値でない値が入力されています");
							    System.exit(0);  /* プログラムを終了する */
							}
							break;
						}
						case 1:{
							try{
							    markovModel.stnum = Integer.parseInt(word);
							}catch(NumberFormatException e){
							    System.out.println("数値でない値が入力されています");
							    System.exit(0);  /* プログラムを終了する */
							}
							markovModel.states = new ArrayList[markovModel.stnum];
							markovModel.initializeState();
							markovModel.deltas = new double[markovModel.stnum][markovModel.stnum];
							markovModel.initializeDelta();
							break;
						}
						case 2:{
							if(stateNum < 0){
								stateNum = Integer.parseInt(word);
								tmp = new ArrayList<Double>();
							}else{
								try{
									markovModel.states[stateNum].add((double)Double.parseDouble(word));
								}catch(NumberFormatException e){
								    System.out.println("数値でない値が入力されています");
								    System.exit(0);  /* プログラムを終了する */
								}
							}
							
							break;
						}
						case 3:{
							if(deltaNum1 < 0 && deltaNum2 < 0){
								deltaNum1 = Integer.parseInt(word);
							}else if(deltaNum1 >=0 && deltaNum2 < 0){
								deltaNum2 = Integer.parseInt(word);
							}else{
								try{
								    markovModel.deltas[deltaNum1][deltaNum2] = Double.parseDouble(word);
								}catch(NumberFormatException e){
								    System.out.println("数値でない値が入力されています");
								    System.exit(0);  /* プログラムを終了する */
								}
							}		
							break;
						}
					}					
				}
			}
			brCSV.close(); //終了処理
		}catch(IOException ex){
			//例外発生時処理
			ex.printStackTrace();
		}
		
	}
	
	public void putModel(){
		//モデルを出力
		putSigma();
		putStnum();
		for(int i=0; i<markovModel.stnum; i++){
			putState(i , markovModel.states[i]);
		}
		for(int i=0; i<markovModel.stnum; i++){
			for(int j=0; j<markovModel.stnum; j++){
				putDelta(i, j, markovModel.deltas[i][j]);
			}
		}
	}
	
	public void putInference(){
		//状態遷移の推論を出力
		System.out.print("Inference:");
		System.out.println(markovModel.ans);
	}
	
	public void putChangingStatus(){
		//実際の状態遷移を出力
		System.out.print("status:");
		System.out.println(markovModel.statusAns);
	}
	
	public void putSigmaAns(){
		//記号列のみ出力
		System.out.print("sigmas:");
		System.out.println(markovModel.sigmaAns);
	}
	
	public void putSS(){
		//記号列と状態の遷移を並べて出力
		System.out.println("=======実行した結果========");
		putSigmaAns();
		putChangingStatus();
		System.out.println("=======実行した結果========");
	}
	
	public void putStates(){
		for(int i=0; i<markovModel.stnum; i++){
			System.out.println("state " + i + ":" + markovModel.states[i]);
		}
	}
	
	private void putSigma(){
		System.out.print("sigma:");
		for(int i=0; i<markovModel.sigma.size(); i++){
			System.out.print(markovModel.sigma.get(i) + " ");
		}
		System.out.println("");
	}
	
	private void putStnum(){
		System.out.println("stnum:" + markovModel.stnum);		
	}
	
	private void putState(int index, ArrayList<Double> state){
		System.out.print("state:" + index + " ");
		for(int i=0; i<state.size(); i++){
			System.out.print(state.get(i) + " ");
		}
		System.out.println("");
	}
	
	private void putDelta(int index1, int index2, Double delta){
		System.out.println("delta:" + index1 + " " + index2 + " " + delta);
	}
	
	
	//学習後の結果を表示
	public void putStudy(){
		putSigma();
		putStnum();
		for(int i=0; i<markovModel.stnum; i++){
			System.out.println(i + ":" + markovModel.inf_states.get(i));
		}
		for(int i=0; i<markovModel.stnum; i++){
			for(int j=0; j<markovModel.stnum; j++){
				putDelta(i, j, markovModel.inf_deltas[i][j]);
			}
		}
	}
	
	public void putData(){
		System.out.println("======モデルの各パラメータカウント結果======");
		putSigma();
		putStnum();
		putStatusCount();
		putSigmaCount();
		putdeltaCount();
		System.out.println("======モデルの各パラメータカウント結果======");
	}
	
	public void putAns(){
		System.out.println("======モデルの各パラメータ予想結果======");
		putInfStates();
		putInfDeltas();
		System.out.println("======モデルの各パラメータ予想結果======");
	}
	
	
	private void putStatusCount(){
		for(int i=0; i<markovModel.stnum; i++){
			System.out.print("status:" + i);
			System.out.println(" " + markovModel.stateCounter.get(i));
		}
	}
	
	private void putSigmaCount(){
		for(int i=0; i<markovModel.sigmaCounter.size(); i++){
			System.out.print("status:" + markovModel.sigmaCounter.get(i).status);
			System.out.print(" sigma:" + markovModel.sigmaCounter.get(i).sigma );
			System.out.println(" " + markovModel.sigmaCounter.get(i).count);
		}
	}
	
	private void putdeltaCount(){
		for(int i=0; i<markovModel.stnum; i++){
			for(int j=0; j<markovModel.stnum; j++){
				System.out.print("delta[" + i + "][" + j + "]:" + markovModel.deltaCounter[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	public void putInfStates(){
		for(int i=0; i<markovModel.inf_states.size(); i++){
			System.out.print("state " + (i) + ":");
			System.out.println(markovModel.inf_states.get(i));		
			}
	}
	
	public void putInfDeltas(){
		for(int i=0; i<markovModel.stnum; i++){
			for(int j=0; j<markovModel.stnum; j++){
				System.out.println("delta " + i + " " + j + " : " + markovModel.inf_deltas[i][j]);
			}
		}
	}
	
	

}
