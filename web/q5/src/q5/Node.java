package q5;

import java.util.ArrayList;
import java.util.List;

public class Node {
	public List<Integer> s_predicates;
	public List<Integer> s_objects;
	public List<Integer> p_subjects;
	public List<Integer> p_objects;
	public List<Integer> o_subjects;
	public List<Integer> o_predicates;
	
	
	public Node(){
		s_predicates = new ArrayList<Integer>();
		s_objects = new ArrayList<Integer>();
		p_subjects = new ArrayList<Integer>();
		p_objects = new ArrayList<Integer>();
		o_subjects = new ArrayList<Integer>();
		o_predicates = new ArrayList<Integer>();
	}
	
	public void s_addChild(int p, int o){
		s_predicates.add(p);
		s_objects.add(o);
	}
	
	public void p_addChild(int s, int o){
		p_subjects.add(s);
		p_objects.add(o);
	}
}
