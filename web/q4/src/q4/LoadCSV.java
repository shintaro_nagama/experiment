package q4;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class LoadCSV {
	//CSV読み込み
	static List<String> uri = new ArrayList<String>();
	static Set<List<Integer>> triples = new HashSet<List<Integer>>();
	
	public static void main(String args[]) throws IOException{
		InputStreamReader is = new InputStreamReader(System.in);
		BufferedReader brFileName = new BufferedReader(is);
		
		System.out.println("読み込むファイル名を入力してください");
		String fileName = brFileName.readLine();
		brFileName.close();
		
		//読み込むファイルはプロジェクトの下のfileフォルダに入れておく
		try{
			//ファイルを読み込む
			FileReader fr = new FileReader(fileName);
			BufferedReader brCSV = new BufferedReader(fr);
			
			//読み込んだファイルを1行ずつ処理する
			String line;
			StringTokenizer token;
			
			while((line = brCSV.readLine()) != null){
				//区切り文字","で分割する
				List<Integer> tmp = new ArrayList<Integer>();
				token = new StringTokenizer(line, ",");
				
				//分割した文字をとりあえず画面に出力する
				while(token.hasMoreTokens()){
					String word = token.nextToken();
					if(uri.indexOf(word) == -1){
						uri.add(word);
					}
					tmp.add(uri.indexOf(word));
					
				}
				triples.add(tmp);
			}
			brCSV.close(); //終了処理
		}catch(IOException ex){
			//例外発生時処理
			ex.printStackTrace();
		}	
		putList();
		System.out.println(triples);
			
	}
	
	public static void putList(){
		for(int i=0; i<uri.size(); i++){
			System.out.print(i+":");
			System.out.println(uri.get(i));
		}
	}

	

}
